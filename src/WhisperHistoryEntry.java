import java.rmi.RemoteException;

public class WhisperHistoryEntry extends HistoryEntry {

    private String destination;

    public WhisperHistoryEntry(String _sender, String _destination, String _message) {
        super(_sender, _message);
        destination = _destination;
    }

    public String toTextMessage() throws RemoteException {
        StringBuilder builder = getBuilderWithDateAndUsername();
        builder.append(" : whispered to " + destination + ": ");
        builder.append(message);
        String retVal = builder.toString();
        return retVal;
    }

    public boolean CanDeliverThisToClient(IClient recepient) throws RemoteException {
        return recepient.getUserName().equals(destination);
    }
}
