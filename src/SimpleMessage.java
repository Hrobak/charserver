import java.io.Serializable;
import java.rmi.RemoteException;

public class SimpleMessage implements IMessage, Serializable {
    private String message;
    SimpleMessage(String _message) {
        message = _message;
    }
    public String toTextMessage() throws RemoteException {
        return message;
    }

}
