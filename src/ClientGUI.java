import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.UUID;

public class ClientGUI extends Frame implements INotified {

    private Container loginScreenContainer;
    private Container chatScreenContainer;
    private Client client;
    private IConnMgr connMgr;
    private TextArea chatArea;
    private TextField chatMessage;
    private Choice broadcastOrWhisper;
    private Choice whisperToUser;
    private Button sendMessage;
    private Button leaveChat;
    private Button displayHistory;
    private Label userAlreadyRegistered;

    static final String BROADCAST = "Broadcast";
    static final String WHISPER = "Whisper";
    static final String LEAVE = "Leave";

    private void proceedToChatScreen() {
        loginScreenContainer.setVisible(false);
        remove(loginScreenContainer);
        add(chatScreenContainer);
        chatScreenContainer.setVisible(true);
    }

    private void proceedToLoginScreen() {
        chatScreenContainer.setVisible(false);
        chatArea.setText("");
        remove(chatScreenContainer);
        userAlreadyRegistered.setVisible(false);
        add(loginScreenContainer);
        loginScreenContainer.setVisible(true);

    }

    public void Action(String notification) {
        chatArea.append(notification + "\n");
        chatArea.setCaretPosition(chatArea.getText().length());
        chatArea.repaint();
    }

    public void NotifyUsersChanged(Iterable<IClient> usersList) {
        try {
            whisperToUser.removeAll();
            for (IClient cl : usersList) {
                whisperToUser.add(cl.getUserName());
            }
            whisperToUser.select(0);
        } catch (RemoteException e){
            e.printStackTrace();
        }
    }

    private void setupActionListeners() {
        broadcastOrWhisper.addItemListener(e -> {
            if (((String)e.getItem()).equals(BROADCAST)) {
                whisperToUser.setEnabled(false);
            } else {
                whisperToUser.setEnabled(true);
            }
        });

        sendMessage.addActionListener(e -> {
            try {
                if (broadcastOrWhisper.getSelectedItem().equals(BROADCAST)) {
                    connMgr.ExecuteCommand(client, "/broadcast " + chatMessage.getText());
                } else {
                    connMgr.ExecuteCommand(client, "/whisper " + whisperToUser.getSelectedItem() + " " + chatMessage.getText());
                }
            } catch (RemoteException exc) {

            }
        });

        leaveChat.addActionListener(e -> {
            try {
                connMgr.ExecuteCommand(client, "/leave");
                proceedToLoginScreen();
            } catch (RemoteException exc) {

            }
        });
    }

    private void createChatScreenComponents() {
        chatScreenContainer = new Container();

        chatArea = new TextArea("", 100, 80);
        chatArea.setEditable(false);
        chatArea.setBounds(10, 30, 500, 300);
        chatScreenContainer.add(chatArea);

        chatMessage = new TextField("", 80);
        chatMessage.setBounds(chatArea.getX(), chatArea.getY() + chatArea.getHeight() + 10, chatArea.getWidth(), 40);
        chatScreenContainer.add(chatMessage);

        broadcastOrWhisper = new Choice();
        broadcastOrWhisper.add(BROADCAST);
        broadcastOrWhisper.add(WHISPER);
        broadcastOrWhisper.setBounds(10, chatMessage.getY() + chatMessage.getHeight(), 90, 20);
        chatScreenContainer.add(broadcastOrWhisper);

        whisperToUser = new Choice();
        //whisperToUser.add(client.getUserName());
        whisperToUser.setBounds(broadcastOrWhisper.getX() + broadcastOrWhisper.getWidth(), broadcastOrWhisper.getY(), 90, 20);
        chatScreenContainer.add(whisperToUser);
        whisperToUser.setEnabled(false);

        sendMessage = new Button("Send");
        sendMessage.setBounds(whisperToUser.getX() + whisperToUser.getWidth(),
                    whisperToUser.getY(), 80, 20);
        chatScreenContainer.add(sendMessage);

        leaveChat = new Button("Leave");
        leaveChat.setBounds(sendMessage.getX() + sendMessage.getWidth(), sendMessage.getY(), 80, 20);
        chatScreenContainer.add(leaveChat);

        displayHistory = new Button("History");
        displayHistory.setBounds(leaveChat.getX() + leaveChat.getWidth(), leaveChat.getY(), 80, 20);
        chatScreenContainer.add(displayHistory);

        chatScreenContainer.setSize(600, 500);
        setupActionListeners();
    }

    public ClientGUI(IConnMgr _connMgr) {

        connMgr = _connMgr;

        this.addWindowListener(new WindowAdapter() {
                                   public void windowClosing(WindowEvent we) {
                                       try {
                                           connMgr.ExecuteCommand(client, "/leave");
                                           proceedToLoginScreen();
                                       } catch (RemoteException exc) {

                                       }
                                       System.exit(0);
                                   }
                               }
        );

        loginScreenContainer = new Container();
        loginScreenContainer.setVisible(false);
        createChatScreenComponents();

        TextField userNameField = new TextField();
        userNameField.setBounds(10, 30, 80, 20);

        Button okUserNameBtn = new Button("OK");
        okUserNameBtn.setBounds(10, 60, 50, 20);

        userAlreadyRegistered = new Label("This username is already used");
        userAlreadyRegistered.setBounds(okUserNameBtn.getX(), okUserNameBtn.getY() + okUserNameBtn.getHeight() + 10, 140, 20);
        userAlreadyRegistered.setVisible(false);

        ClientGUI ptr = this;
        okUserNameBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    String userInput = userNameField.getText();
                    System.out.println(userInput);
                    if (userInput.isEmpty()) {
                        /* Do nothing */
                        System.out.println("FAILURE");
                    } else {
                        client = new Client(UUID.randomUUID().toString(), userNameField.getText(), ptr);
                        int regResult = connMgr.RegisterClient(client);
                        if (regResult == 0) {
                            System.out.println("Success 1");
                            proceedToChatScreen();
                        } else {
                            System.out.println(regResult);
                            userAlreadyRegistered.setVisible(true);
                        }
                    }
                } catch (Exception e) {

                }
            }
        });

        loginScreenContainer.add(userNameField);
        loginScreenContainer.add(okUserNameBtn);
        loginScreenContainer.add(userAlreadyRegistered);
        loginScreenContainer.setSize(200, 200);

        add(loginScreenContainer);
        loginScreenContainer.setVisible(true);

        setSize(600, 500);
        setLayout(null);
        setVisible(true);
    }
}
