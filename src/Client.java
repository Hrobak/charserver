import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Client implements Serializable, IClient {
    private String id;
    private String userName;
    private INotified notified;

    public Client(String _id, String _userName, INotified _notified) throws RemoteException {
        UnicastRemoteObject.exportObject(this, 0);
        id = _id;
        userName = _userName;
        notified = _notified;
    }

    public void notifyUsersChanged(Iterable<IClient> users) throws RemoteException {
        notified.NotifyUsersChanged(users);
    }

    public  String getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public void deliverMessage(IMessage message) throws RemoteException {
//        System.out.println(message.toTextMessage());

        if (notified != null) {
            notified.Action(message.toTextMessage());
        }
    }

}
