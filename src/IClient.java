import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IClient extends Remote{
    public String getId() throws RemoteException;
    public String getUserName() throws RemoteException;
    public void deliverMessage(IMessage message) throws RemoteException;
    public void notifyUsersChanged(Iterable<IClient> users) throws RemoteException;
}
