import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class HistoryEntry implements IMessage, Serializable {
    //protected IClient client;
    protected String client;
    protected String message;
    protected LocalDateTime timestamp;

    public HistoryEntry(String _client, String _message) {
        client = _client;
        message = _message;
        timestamp = LocalDateTime.now();
    }

    public String getClient() {
        return client;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    protected StringBuilder getBuilderWithDateAndUsername() throws RemoteException {
        StringBuilder builder = new StringBuilder();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedTimeStamp = timestamp.format(formatter);

        builder.append("[");
        builder.append(formattedTimeStamp);
        builder.append("] ");
        if (client != null) {
            builder.append(client);
        }
        return builder;
    }

    public String toTextMessage() throws RemoteException {
        StringBuilder builder = getBuilderWithDateAndUsername();
        builder.append(" broadcasted: ");
        builder.append(message);

        String retVal = builder.toString();
//        System.out.print("Converted message : ");
//        System.out.println(retVal);
        return retVal;
    }

    public boolean CanDeliverThisToClient(IClient recepient) throws RemoteException {
        return true;
    }

}
