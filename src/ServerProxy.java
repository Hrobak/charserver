import java.net.InetAddress;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServerProxy {
    public static void main(String[] args) {
        try {
//            Process p = Runtime.getRuntime().exec("rmiregistry");
//            Runtime.getRuntime().addShutdownHook(new Thread()
//            {
//                @Override
//                public void run()
//                {
//                    System.out.println("Shutdown hook ran!");
//                    p.destroy();
//                }
//            });
            //System.setProperty( "java.rmi.server.hostname", "192.168.RMIServer.IP" ) ;
            System.setProperty("java.rmi.server.hostname", InetAddress.getLocalHost().getHostAddress());

            LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
            ConnMgr connMgr = new ConnMgr();
            IConnMgr imgr_stub = (IConnMgr) UnicastRemoteObject.exportObject(connMgr, Registry.REGISTRY_PORT);
            Registry registry = LocateRegistry.getRegistry();

            registry.rebind("ConnectionService", imgr_stub);


        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
