import java.rmi.Remote;
import java.rmi.RemoteException;

public interface INotified extends Remote{
    public void Action(String notification) throws RemoteException;
    public void NotifyUsersChanged(Iterable<IClient> usersList) throws RemoteException;
}
