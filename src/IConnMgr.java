import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IConnMgr extends Remote {

    /*!
        \return 0   < if registration was successful
     */
    public int RegisterClient(IClient client) throws RemoteException;
    public void ExecuteCommand(IClient sender, String commandWithMessage) throws RemoteException;
    public Iterable<String> GetConnectedUsers() throws RemoteException;
}
