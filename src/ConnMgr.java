import java.io.*;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;

public class ConnMgr implements IConnMgr, Remote {

    private ArrayList<IClient> registeredClients = new ArrayList<>();
    private ArrayList<HistoryEntry> history = new ArrayList<>();

    static final String BROADCAST_COMMAND = "/broadcast";
    static final String WHISPER_COMMAND = "/whisper";
    static final String GET_USERS_COMMAND = "/getusers";
    static final String DUMP_FILE_NAME = "server.ser";
    static final String LEAVE_COMMAND = "/leave";

    public ConnMgr() {
        RetrieveHistoryFromFile();
    }

    private IClient getClientByName(String userName) throws RemoteException {
        for (IClient rClient : registeredClients) {
            if (rClient.getUserName().equals(userName)) {
                return rClient;
            }
        }
        return null;
    }

    public int RegisterClient(IClient client) throws RemoteException {
        for (IClient rClient:
             registeredClients) {
            if (rClient.getId().equals(client.getId())) {
                return -1;
            }

            if (rClient.getUserName().equals(client.getUserName())) {
                return -2;
            }
        }

        registeredClients.add(client);

        for(HistoryEntry entry : history) {
            if (entry.CanDeliverThisToClient(client)) {
                client.deliverMessage(entry);
            }
        }

        //TODO: Will fail if we have multiple rooms
        SendMessageInCurrentRoom(null, "User " + client.getUserName() + " joined the room.");


        for (IClient rClient : registeredClients) {
            rClient.notifyUsersChanged(registeredClients);
        }

        return 0;
    }

    public void SendMessageInCurrentRoom(IClient sender, String message) throws RemoteException {
        HistoryEntry historyEntry = new HistoryEntry(sender == null ? null : sender.getUserName(), message);
        for (IClient rClient: registeredClients) {
            rClient.deliverMessage(historyEntry);
            /*
            if ((sender != null) && (!rClient.getId().equals(sender.getId()))) {
                rClient.deliverMessage(historyEntry);
            }
            */
        }
        //history.add(historyEntry);
        AddToHistoryAndSave(historyEntry);
    }

    public void SendMessageTo(IClient sender, IClient receiver, String message) throws RemoteException {
        WhisperHistoryEntry historyEntry = new WhisperHistoryEntry(sender.getUserName(), receiver.getUserName(), message);
        receiver.deliverMessage(historyEntry);
        //history.add(historyEntry);
        AddToHistoryAndSave(historyEntry);

    }

    public void ExecuteCommand(IClient sender, String commandWithMessage) throws RemoteException {
        String[] parsedCommand = commandWithMessage.split(" ");

        //TODO: Refactor to use factory or some nice stuff
        if (parsedCommand[0].equals(BROADCAST_COMMAND)) {
            if (parsedCommand.length < 2) {
                //TODO: Report an error
            }
            System.out.println("Sending message in the current room " + sender.getUserName() + " " + parsedCommand[1]);
            SendMessageInCurrentRoom(sender, String.join(" ", Arrays.copyOfRange(parsedCommand, 1, parsedCommand.length)));
        } else if (parsedCommand[0].equals(WHISPER_COMMAND)) {
            if (parsedCommand.length < 3) {
                //TODO: report an error
            }

            IClient receiver = getClientByName(parsedCommand[1]);
            if (receiver != null) {
                SendMessageTo(sender, receiver, String.join(" ", Arrays.copyOfRange(parsedCommand, 2, parsedCommand.length)));
            } else {
                //TODO: Report an error
            }
        } else if (parsedCommand[0].equals(GET_USERS_COMMAND)) {
            StringBuilder builder = new StringBuilder();
            for (String uName : GetConnectedUsers()) {
                builder.append(uName);
                builder.append("\n");
            }
            sender.deliverMessage(new SimpleMessage(builder.toString()));
        } else if (parsedCommand[0].equals(LEAVE_COMMAND)) {
            registeredClients.remove(sender);
            SendMessageInCurrentRoom(null, "User " + sender.getUserName() + " left the room.");
            for (IClient rClient : registeredClients) {
                rClient.notifyUsersChanged(registeredClients);
            }
        } else {
            System.out.println("Unknown command");

            //TODO: Report to the client
        }

    }

    public Iterable<String> GetConnectedUsers() throws RemoteException {
        ArrayList<String> usernames = new ArrayList<>();
        for (IClient rClient : registeredClients) {
            usernames.add(rClient.getUserName());
        }
        return usernames;
    }

    private void DumpToFileSystem() throws Exception {
        FileOutputStream fos = new FileOutputStream(DUMP_FILE_NAME);
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        Integer numEntries = history.size();
        oos.writeObject(numEntries);
        for (HistoryEntry hEntry : history) {
            oos.writeObject(hEntry);
        }

        oos.close();
    }

    private void AddToHistoryAndSave(HistoryEntry entry) {
        try {
            history.add(entry);
            DumpToFileSystem();
        } catch (Exception e) {
            /* do nothing */
        }
    }

    private void RetrieveHistoryFromFile() {
        try {
            FileInputStream fis = new FileInputStream(DUMP_FILE_NAME);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Integer numEntries = (Integer)ois.readObject();

            for (int ii = 0; ii < numEntries; ++ii) {
                HistoryEntry hEntry = (HistoryEntry)ois.readObject();
                history.add(hEntry);
            }
        } catch (Exception e) {
            // Exit
            return;
        }


    }

}
